﻿using DiwaTouch.UserProfile.Persistence.Port.Contracts;
using DiwaTouch.UserProfile.Persistence.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace DiwaTouch.UserProfile.Persistence;

public static class Extensions
{
    public static IServiceCollection AddUserProfilePersistence(this IServiceCollection service)
    {
        service.AddDbContext<AppDbContext>();
        service.AddScoped<IUserProfileRepository, UserProfileRepository>();
        
        return service;
    }
}