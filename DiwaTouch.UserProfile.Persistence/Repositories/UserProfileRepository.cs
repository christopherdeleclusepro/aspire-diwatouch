﻿using DiwaTouch.UserProfile.Persistence.Port.Contracts;
using DiwaTouch.UserProfile.Persistence.Port.Entities;
using MongoDB.Driver;

namespace DiwaTouch.UserProfile.Persistence.Repositories;

internal sealed class UserProfileRepository(AppDbContext context) : IUserProfileRepository
{
    public async Task CreateProfileAsync(UserProfileEntity userProfile) =>
        await context.UserProfile.InsertOneAsync(userProfile);

    public async Task<UserProfileEntity> GetProfileByEmailAsync(string email) =>
        await context.UserProfile
            .Find(u => u.Email == email)
            .FirstOrDefaultAsync();

    public async Task UpdateIsDeletedAsync(string email, bool isDeleted) =>
        await context.UserProfile.UpdateOneAsync(u => u.Email == email,
            Builders<UserProfileEntity>.Update
                .Set(u => u.IsDeleted, isDeleted)
        );
}