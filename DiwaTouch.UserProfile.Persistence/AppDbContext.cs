﻿using DiwaTouch.UserProfile.Persistence.Port.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;

namespace DiwaTouch.UserProfile.Persistence;

public sealed class AppDbContext : DbContext
{
    private readonly IMongoDatabase _database;

    public AppDbContext(DbContextOptions<AppDbContext> options, IConfiguration configuration): base(options)
    {
        var client = new MongoClient(configuration.GetConnectionString("Default"));
        _database = client.GetDatabase(configuration.GetSection("Database:UserProfile").Value);
    }

    public IMongoCollection<UserProfileEntity> UserProfile => _database.GetCollection<UserProfileEntity>("Profile");
}
