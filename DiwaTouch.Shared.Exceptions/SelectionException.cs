﻿namespace DiwaTouch.Shared.Exceptions;

public class SelectionException(string message) : Exception(message)
{
    public static SelectionException ForNotFound() => new($"Selection Error: Item was not found.");
}