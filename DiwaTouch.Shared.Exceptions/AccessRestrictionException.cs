﻿namespace DiwaTouch.Shared.Exceptions;

public class AccessRestrictionException(string message) : Exception(message)
{
    public static AccessRestrictionException ForNotVerifiedAccount() => new("Account is not verified.");
}