﻿namespace DiwaTouch.Shared.Exceptions;

public class UnAuthorizedException(string message) : Exception(message)
{
    public static UnAuthorizedException ForInvalidCredentials() => new("Invalid credentials.");
}
