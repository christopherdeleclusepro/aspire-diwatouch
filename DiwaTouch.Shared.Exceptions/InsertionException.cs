﻿namespace DiwaTouch.Shared.Exceptions;

public class InsertionException(string message) : Exception(message)
{
    public static InsertionException ForFailed(string message) => new($"Insertion Error: {message}");

    public static InsertionException ForSameValue(string propertyName) =>
        new($"Insertion Error: attempting to insert the same value for the property '{propertyName}'.");
}