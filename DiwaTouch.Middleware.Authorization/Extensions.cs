﻿using System.Security.Claims;
using System.Text;
using DiwaTouch.Middleware.Authorization.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.IdentityModel.Tokens;

namespace DiwaTouch.Middleware.Authorization;

public static class Extensions
{
    public static IServiceCollection AddCustomAuthenticationAndAuthorization(
        this IServiceCollection service,
        IConfiguration configuration
    )
    {
        service.AddAuthentication(opt =>
                {
                    opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    opt.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                }
            )
            .AddJwtBearer(jwtOpt =>
                {
                    jwtOpt.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidIssuer = configuration["JwtOptions:Issuer"],
                        ValidAudience = configuration["JwtOptions:Audience"],
                        IssuerSigningKey =
                            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JwtOptions:Secret"]!)),
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true
                    };
                }
            );

        service.AddAuthorization();

        return service;
    }

    public static void UseCustomAuthenticationAndAuthorization(this WebApplication app)
    {
        app.UseAuthentication();
        app.UseAuthorization();
    }

    public static TokenUserClaims GetUserClaims(this HttpContext context) =>
        new(context.User.FindFirstValue(JwtRegisteredClaimNames.Jti),
            context.User.FindFirstValue(ClaimTypes.Email),
            context.User.FindFirstValue(JwtRegisteredClaimNames.Name));
}