﻿namespace DiwaTouch.Middleware.Authorization.Models;

public record TokenUserClaims(string? Jti, string? Email, string? Name);
