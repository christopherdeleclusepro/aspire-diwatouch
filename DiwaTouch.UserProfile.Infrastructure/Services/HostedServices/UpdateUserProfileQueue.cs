﻿using DiwaTouch.Messaging.Port;
using DiwaTouch.Messaging.Port.Abstractions;
using DiwaTouch.Messaging.Port.Contracts;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace DiwaTouch.UserProfile.Infrastructure.Services.HostedServices;

public class UpdateUserProfileQueue(
    IServiceScopeFactory serviceScopeFactory,
    IEventConsumer eventConsumer,
    ILogger<UpdateUserProfileQueue> logger
) : GenericQueueHostedService<UpdateUserProfileQueue>(serviceScopeFactory, eventConsumer, QueueNames.UpdateUserProfile, logger);