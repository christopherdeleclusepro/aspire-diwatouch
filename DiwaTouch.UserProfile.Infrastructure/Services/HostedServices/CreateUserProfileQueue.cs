﻿using DiwaTouch.Messaging.Port;
using DiwaTouch.Messaging.Port.Abstractions;
using DiwaTouch.Messaging.Port.Contracts;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace DiwaTouch.UserProfile.Infrastructure.Services.HostedServices;

public class CreateUserProfileQueue(
    IServiceScopeFactory serviceScopeFactory,
    IEventConsumer eventConsumer,
    ILogger<CreateUserProfileQueue> logger
) : GenericQueueHostedService<CreateUserProfileQueue>(serviceScopeFactory, eventConsumer, QueueNames.CreateUserProfile, logger);