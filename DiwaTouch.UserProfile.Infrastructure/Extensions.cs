﻿using DiwaTouch.Messaging.Port.Contracts;
using DiwaTouch.Messaging.Port.Services;
using DiwaTouch.Middleware.Authorization;
using DiwaTouch.UserProfile.Infrastructure.Services.HostedServices;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DiwaTouch.UserProfile.Infrastructure;

public static class Extensions
{
    public static void AddUserProfileInfrastructure(this IServiceCollection service, IConfiguration configuration)
    {
        service.AddHostedService<CreateUserProfileQueue>();
        service.AddHostedService<UpdateUserProfileQueue>();
        
        service.AddSingleton<IEventPublisher, RabbitmqEventPublisherService>();
        service.AddSingleton<IEventConsumer, RabbitmqEventConsumerService>();
        
        service.AddCustomAuthenticationAndAuthorization(configuration);
    }

    public static void UseInfrastructureAuthorization(this WebApplication app)
    {
        app.UseCustomAuthenticationAndAuthorization();
    }
}