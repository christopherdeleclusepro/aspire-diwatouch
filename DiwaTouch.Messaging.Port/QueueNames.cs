﻿namespace DiwaTouch.Messaging.Port;

public abstract class QueueNames
{
    public const string CreateIdentityUser = "create.identity.user";
    
    public const string UpdateIdentityUser = "update.identity.user";

    public const string CreateUserProfile = "create.user.profile";
    
    public const string UpdateUserProfile = "update.user.profile";
    
    public const string UserInfoRequest = "userprofile.info.request";

    public const string UserInfoResponse = "userprofile.info.response";

    public const string EmailConfirmedAccount = "email.confirmed.account";
    
    public const string EmailChangeAccountState = "email.change.account.state";
}
