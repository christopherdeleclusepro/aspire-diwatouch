﻿using DiwaTouch.Messaging.Port.Abstractions;

namespace DiwaTouch.Messaging.Port.Events.UserProfile;

public record ChangeUserProfileStateEvent(string Email, bool IsActive)
    : BaseEvent(nameof(ChangeUserProfileStateEvent));