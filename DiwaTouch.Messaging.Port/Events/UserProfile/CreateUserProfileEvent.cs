﻿using DiwaTouch.Messaging.Port.Abstractions;

namespace DiwaTouch.Messaging.Port.Events.UserProfile;

public record CreateUserProfileEvent(
    string Email,
    string FirstName,
    string LastName,
    string Gender,
    DateTime BirthDate
) : BaseEvent(nameof(CreateUserProfileEvent));