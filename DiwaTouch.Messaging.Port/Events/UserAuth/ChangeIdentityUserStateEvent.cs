﻿using DiwaTouch.Messaging.Port.Abstractions;

namespace DiwaTouch.Messaging.Port.Events.UserAuth;

public record ChangeIdentityUserStateEvent(string Email, bool IsActive)
    : BaseEvent(nameof(ChangeIdentityUserStateEvent));