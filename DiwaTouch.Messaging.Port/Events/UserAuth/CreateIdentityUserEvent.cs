﻿using DiwaTouch.Messaging.Port.Abstractions;

namespace DiwaTouch.Messaging.Port.Events.UserAuth;

public record CreateIdentityUserEvent(
    string Email,
    string FirstName,
    string LastName,
    string Gender,
    DateTime BirthDate
) : BaseEvent(nameof(CreateIdentityUserEvent));