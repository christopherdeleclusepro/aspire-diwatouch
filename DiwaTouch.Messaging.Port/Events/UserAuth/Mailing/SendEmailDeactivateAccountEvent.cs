﻿using DiwaTouch.Messaging.Port.Abstractions;

namespace DiwaTouch.Messaging.Port.Events.UserAuth.Mailing;

public record SendEmailDeactivateAccountEvent(string Email)
    : BaseEvent(nameof(SendEmailDeactivateAccountEvent));