﻿using DiwaTouch.Messaging.Port.Abstractions;

namespace DiwaTouch.Messaging.Port.Events.UserAuth.Mailing;

public record SendEmailReactivateAccountEvent(string Email)
    : BaseEvent(nameof(SendEmailReactivateAccountEvent));