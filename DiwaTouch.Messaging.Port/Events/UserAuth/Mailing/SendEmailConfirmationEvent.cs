﻿using DiwaTouch.Messaging.Port.Abstractions;

namespace DiwaTouch.Messaging.Port.Events.UserAuth.Mailing;

public record SendEmailConfirmationEvent(
    string UserId,
    string Email,
    string FirstName,
    string LastName,
    string ConfirmationToken)
    : BaseEvent(nameof(SendEmailConfirmationEvent));