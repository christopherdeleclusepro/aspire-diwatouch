﻿using System.Text;
using System.Text.Json;
using DiwaTouch.Messaging.Port.Abstractions;
using DiwaTouch.Messaging.Port.Contracts;
using RabbitMQ.Client;

namespace DiwaTouch.Messaging.Port.Services;

public class RabbitmqEventPublisherService(IConnection connection)
    : RabbitmqEventServiceBase(connection), IEventPublisher
{
    public void PublishEvent<T>(string queueName, T message) where T : IMessageBus
    {
        ArgumentNullException.ThrowIfNull(message);
        ArgumentException.ThrowIfNullOrEmpty(queueName);

        var channel = GetChannel();
        channel.QueueDeclare(queueName, exclusive: false);

        var json = JsonSerializer.Serialize(message);
        var body = Encoding.UTF8.GetBytes(json);
        
        channel.BasicPublish("", queueName, body: body);
    }
}
