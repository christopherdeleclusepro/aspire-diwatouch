﻿using DiwaTouch.Messaging.Port.Abstractions;
using DiwaTouch.Messaging.Port.Contracts;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace DiwaTouch.Messaging.Port.Services;

public class RabbitmqEventConsumerService(IConnection connection)
    : RabbitmqEventServiceBase(connection), IEventConsumer
{
    public void SubscribeEvent(string queueName, Action<object?, BasicDeliverEventArgs> onReceivedMessage)
    {
        ArgumentNullException.ThrowIfNull(onReceivedMessage);

        var channel = GetChannel();
        channel.QueueDeclare(queueName, exclusive: false);

        var consumer = new EventingBasicConsumer(channel);
        consumer.Received += (model, eventArgs) => onReceivedMessage(model, eventArgs);

        channel.BasicConsume(queue: queueName, autoAck: true, consumer: consumer);
    }
}
