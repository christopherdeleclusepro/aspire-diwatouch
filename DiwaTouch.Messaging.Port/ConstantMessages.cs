﻿namespace DiwaTouch.Messaging.Port;

internal abstract class ConstantMessages
{
    public const string UnknownError = "An error occured while proccessing the message";
    
    public const string JsonError = "Error during JSON deserialization";
    
    public const string EventMessageNull = "[MessageBusEvent] is null. Event not published";
    
    public const string UnknownEventType = "Unknown event type";
    
    public const string EventPublished = "Event published successfully";
}