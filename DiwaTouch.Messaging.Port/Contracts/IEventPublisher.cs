﻿namespace DiwaTouch.Messaging.Port.Contracts;

public interface IEventPublisher
{
    void PublishEvent<TEvent>(string queueName, TEvent message) where TEvent : IMessageBus;
}