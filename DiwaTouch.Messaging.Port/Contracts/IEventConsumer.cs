﻿using RabbitMQ.Client.Events;

namespace DiwaTouch.Messaging.Port.Contracts;

public interface IEventConsumer
{
    public void SubscribeEvent(string queueName, Action<object?, BasicDeliverEventArgs> onReceiveMessage);
}