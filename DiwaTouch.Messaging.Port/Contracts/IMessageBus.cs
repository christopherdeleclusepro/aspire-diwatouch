﻿using MediatR;

namespace DiwaTouch.Messaging.Port.Contracts;

public interface IMessageBus : INotification;
