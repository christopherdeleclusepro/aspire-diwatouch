﻿using DiwaTouch.Messaging.Port.Contracts;

namespace DiwaTouch.Messaging.Port.Abstractions;

public record BaseEvent(string EventName): IMessageBus;
