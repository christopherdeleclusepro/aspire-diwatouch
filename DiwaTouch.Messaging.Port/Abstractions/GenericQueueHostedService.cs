﻿using System.Text;
using System.Text.Json;
using DiwaTouch.Messaging.Port.Contracts;
using DiwaTouch.Messaging.Port.Events;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client.Events;

namespace DiwaTouch.Messaging.Port.Abstractions;

public class GenericQueueHostedService<TQueueService>(
    IServiceScopeFactory serviceScopeFactory,
    IEventConsumer eventConsumer,
    string queueName,
    ILogger<TQueueService> logger) : BackgroundService
{
    private readonly Dictionary<string, Type> _eventTypes = typeof(BaseEvent).Assembly.DefinedTypes
        .Where(@class => typeof(IMessageBus).IsAssignableFrom(@class) &&
                         @class is { IsInterface: false, IsAbstract: false }
        )
        .ToDictionary(type => type.Name, type => type.AsType());


    protected override Task ExecuteAsync(CancellationToken stoppingToken)
    {
        eventConsumer.SubscribeEvent(queueName, OnReceiveMessage);

        return Task.Delay(1000, stoppingToken);

        async void OnReceiveMessage(object? _, BasicDeliverEventArgs args)
        {
            var body = args.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);

            try
            {
                var eventName = GetEventName(message);

                if (!_eventTypes.TryGetValue(eventName!, out var eventType))
                {
                    logger.LogWarning("{unknownEventType}: {eventName}", ConstantMessages.UnknownEventType, eventType);
                    return;
                }

                var eventData = JsonSerializer.Deserialize(message, eventType);

                if (eventData is IMessageBus messageBusEvent)
                    await PublishMediatorEvent(messageBusEvent, stoppingToken);
                else
                    logger.LogWarning(ConstantMessages.EventMessageNull);
            }
            catch (JsonException e)
            {
                logger.LogError(e, ConstantMessages.JsonError);
            }
            catch (Exception e)
            {
                logger.LogError(e, ConstantMessages.UnknownError);
            }
        }
    }

    private string? GetEventName(string message) =>
        JsonDocument.Parse(message).RootElement.GetProperty("EventName").GetString();

    private async Task PublishMediatorEvent(IMessageBus messageBusEvent, CancellationToken cancellationToken)
    {
        using var scope = serviceScopeFactory.CreateScope();
        var mediator = scope.ServiceProvider.GetRequiredService<IPublisher>();

        await mediator.Publish(messageBusEvent, cancellationToken);
        logger.LogInformation("{publishSuccessfully}: {message}", ConstantMessages.EventPublished, messageBusEvent);
    }
}