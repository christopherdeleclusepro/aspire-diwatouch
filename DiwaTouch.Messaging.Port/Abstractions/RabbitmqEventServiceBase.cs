﻿using RabbitMQ.Client;

namespace DiwaTouch.Messaging.Port.Abstractions;

public class RabbitmqEventServiceBase(IConnection connection)
{
    private readonly IConnection _connection =
        connection ?? throw new ArgumentNullException(nameof(connection));

    private readonly IModel? _channel = null;

    protected IModel GetChannel() =>
        (_channel is null || _channel.IsClosed)
            ? _connection.CreateModel()
            : _channel;
}
