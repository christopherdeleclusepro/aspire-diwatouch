var builder = DistributedApplication.CreateBuilder(args);

var rabbitmq = builder.AddRabbitMQContainer("messaging");

var identityMssql = builder.AddSqlServerContainer("sql-identity", "DiwaCode2023", port: 1434)
    .AddDatabase("DiwaTouchIdentity");

builder.AddContainer("noSql-userProfile", "mongodb/mongodb-community-server")
    .WithEnvironment("MONGO_INITDB_ROOT_USERNAME", "diwa")
    .WithEnvironment("MONGO_INITDB_ROOT_PASSWORD", "code")
    .WithServiceBinding(containerPort: 27017, hostPort: 27017, name: "userprofile-mongoDb");

builder.AddContainer("event-store", "eventstore/eventstore")
    .WithEnvironment("EVENTSTORE_CLUSTER_SIZE", "1")
    .WithEnvironment("EVENTSTORE_RUN_PROJECTIONS", "All")
    .WithEnvironment("EVENTSTORE_START_STANDARD_PROJECTIONS", "true")
    .WithEnvironment("EVENTSTORE_EXT_TCP_PORT", "1113")
    .WithEnvironment("EVENTSTORE_HTTP_PORT", "2113")
    .WithEnvironment("EVENTSTORE_INSECURE", "true")
    .WithEnvironment("EVENTSTORE_ENABLE_EXTERNAL_TCP", "true")
    .WithEnvironment("EVENTSTORE_ENABLE_ATOM_PUB_OVER_HTTP", "true")
    .WithVolumeMount("eventstore-volume-data", "/var/lib/eventstore", VolumeMountType.Named)
    .WithVolumeMount("eventstore-volume-logs", "/var/log/eventstore", VolumeMountType.Named)
    .WithServiceBinding(containerPort: 1113, hostPort: 1113, name: "DiwaTouch-eventStore")
    .WithServiceBinding(containerPort: 2113, hostPort: 2113, name: "DiwaTouch-eventStore2");


builder.AddProject<Projects.DiwaTouch_Auth_Api>("auth.api")
    .WithReference(rabbitmq)
    .WithReference(identityMssql);

builder.AddProject<Projects.DiwaTouch_UserProfile_Api>("user.profile.api")
    .WithReference(rabbitmq);

builder.AddProject<Projects.DiwaTouch_Service_Chat_Api>("chat.api");

builder.AddProject<Projects.DiwaTouch_Messaging_Api>("messaging.api")
    .WithReference(rabbitmq);

builder.AddProject<Projects.DiwaTouch_Mailing_Api>("mailing.api")
    .WithReference(rabbitmq);

builder.Build().Run();