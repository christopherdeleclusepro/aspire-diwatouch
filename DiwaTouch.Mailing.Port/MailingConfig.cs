﻿namespace DiwaTouch.Shared.Mailing;

public class MailingConfig
{
    public string SenderName { get; set; }
    
    public string SenderEmail { get; set; }
    
    public string ApiKey { get; set; }
}
