﻿using DiwaTouch.Mailing.Port.Abstractions;

namespace DiwaTouch.Mailing.Port.Contracts;

public interface IHtmlToStringParser
{
    Task<string> ParseTemplateHtmlAsync(TemplateNames templateNames, string userId, string firstName, string lastName, string token);
}