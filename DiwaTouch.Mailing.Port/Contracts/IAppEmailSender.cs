﻿namespace DiwaTouch.Shared.Mailing.Abstractions;

public interface IAppEmailSender
{
    public Task SendEmailAsync(string toName, string toEmails, string subject, string htmlContent);
}
