﻿namespace DiwaTouch.Mailing.Port.Abstractions;

public enum TemplateNames
{
    EmailConfirmation,
    ReactivateAccount,
    DeactivateAccount
}