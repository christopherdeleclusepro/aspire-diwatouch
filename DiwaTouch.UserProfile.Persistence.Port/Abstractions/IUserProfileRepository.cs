﻿using DiwaTouch.UserProfile.Persistence.Port.Entities;

namespace DiwaTouch.UserProfile.Persistence.Port.Contracts;

public interface IUserProfileRepository
{
    public Task CreateProfileAsync(UserProfileEntity userProfile);
    
    public Task<UserProfileEntity> GetProfileByEmailAsync(string email);
    
    public Task UpdateIsDeletedAsync(string email, bool isDeleted);
}
