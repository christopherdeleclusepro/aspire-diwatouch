﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DiwaTouch.UserProfile.Persistence.Port.Entities;

public class UserProfileEntity
{
    [BsonId]
    public ObjectId Id { get; set; }

    [BsonElement("email")]
    public string? Email { get; set; }

    [BsonElement("last_name")]
    public string? FirstName { get; set; }

    [BsonElement("first_name")]
    public string? LastName { get; set; }

    [BsonElement("gender")]
    public string? Gender { get; set; }
    
    [BsonElement("birth_date")]
    public DateTime? BirthDate { get; set; }
    
    [BsonElement("country")]
    public string? Country { get; set; }
    
    [BsonElement("has_partner")]
    public bool? HasPartner { get; set; }

    [BsonElement("profile_picture")]
    public string? ProfilePicture { get; set; }

    [BsonElement("backdrop_image")]
    public string? BackdropImage { get; set; }
    
    [BsonElement("is_deleted_account")]
    public bool? IsDeleted { get; set; }
}
