﻿using DiwaTouch.Auth.Infrastructure;
using DiwaTouch.Auth.Persistence;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace DiwaTouch.Auth.DependencyInjections;

public static class Extensions
{
    public static void AddAuthServer(this WebApplicationBuilder builder, IConfiguration configuration)
    {
        builder.AddAuthPersistence(configuration);
        builder.Services.AddAuthDomain(configuration);
        builder.Services.AddAuthInfra(configuration);
        
        builder.Services.AddHttpContextAccessor();
        builder.AddRabbitMQ("messaging");
    }

    public static void UseAuthServer(this WebApplication app)
    {
        app.UseAuthInfra();
        app.UseAuthPersistence();
    }
}