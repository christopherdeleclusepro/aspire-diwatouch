using Carter;
using DiwaTouch.UserProfile.DependencyInjections;
using DiwaTouch.UserProfile.Infrastructure;

var builder = WebApplication.CreateBuilder(args);

builder.AddServiceDefaults();
builder.AddRabbitMQ("messaging");

// Add services to the container.
builder.Services.AddUserProfileDependencyInjections(builder.Configuration);

builder.Services.AddCarter();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
var app = builder.Build();

app.MapDefaultEndpoints();

app.UseInfrastructureAuthorization();

// Configure the HTTP request pipeline.
app.UseHttpsRedirection();

app.MapCarter();

app.Run();
