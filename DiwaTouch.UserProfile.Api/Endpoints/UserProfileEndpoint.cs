﻿using Carter;
using DiwaTouch.UserProfile.Queries;
using MediatR;

namespace DiwaTouch.UserProfile.Api.Endpoints;

public class UserProfileEndpoint : ICarterModule
{
    public void AddRoutes(IEndpointRouteBuilder app)
    {
        var group = app.MapGroup("user-profiles").RequireAuthorization();

        group.MapGet("{email}",
            async (string email, ISender sender) =>
            {
                var result = await sender.Send(new GetUserProfileByEmailQuery(email));

                return result.IsSuccess
                    ? Results.Ok(result.Data)
                    : Results.NotFound($"[{result.Error.Code}]: {result.Error.Description ?? string.Empty}");
            }
        );
    }
}
