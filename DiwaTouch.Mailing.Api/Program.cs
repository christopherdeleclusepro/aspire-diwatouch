using DiwaTouch.Mailing.DependencyInjections;

var builder = WebApplication.CreateBuilder(args);

builder.AddServiceDefaults();
builder.AddRabbitMQ("messaging");

// Add services to the container.
builder.Services.AddMailingService(builder.Configuration);

var app = builder.Build();

app.MapDefaultEndpoints();

// Configure the HTTP request pipeline.

app.UseHttpsRedirection();

app.Run();
