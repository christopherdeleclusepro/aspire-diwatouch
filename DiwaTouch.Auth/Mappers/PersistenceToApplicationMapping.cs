﻿using AutoMapper;
using DiwaTouch.Auth.Models;
using DiwaTouch.Auth.Persistence.Port.Contracts;

namespace DiwaTouch.Auth.Mappers;

public class PersistenceToApplicationMapping : Profile
{
    public PersistenceToApplicationMapping()
    {
        CreateMap<IAppUser, UserModel>();
    }
}