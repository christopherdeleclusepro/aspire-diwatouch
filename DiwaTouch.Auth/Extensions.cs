﻿using System.Reflection;
using FluentValidation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DiwaTouch.Auth;

public static class Extensions
{
    public static void AddAuthDomain(this IServiceCollection service, IConfiguration configuration)
    {
        service.AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(Assembly.GetExecutingAssembly()));
        service.AddAutoMapper(cfg => cfg.AddMaps(Assembly.GetExecutingAssembly()));
        service.AddValidatorsFromAssemblyContaining(typeof(Extensions));
    }
}