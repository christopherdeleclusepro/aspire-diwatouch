﻿using DiwaTouch.Auth.Models;
using DiwaTouch.Shared.OperationResult;

namespace DiwaTouch.Auth.Errors;

public static class AuthError
{
    public static readonly Error<(UserModel, string)> InvalidCredentials = new("UnAuthorized", "Invalid credentials.");
}