﻿using DiwaTouch.Auth.Models;
using DiwaTouch.Shared.OperationResult;

namespace DiwaTouch.Auth.Errors;

public abstract class AccountRestrictionError
{
    public static readonly Error<(UserModel, string)> NotConfirmed = new("Access.Denied", "Account not confirmed.");
}