﻿using DiwaTouch.Shared.OperationResult;
using MediatR;

namespace DiwaTouch.Auth.Errors;

public static class UserError
{
    public static readonly Error<object> IsAlreadyExist = new("Insertion.Error", "Duplicate userName.");

    public static readonly Error<object> IsNotFound = new("Selection.Error", "Item was not found.");

    public static readonly Error<object> InvalidPhoneNumberFormat = new("Update.Error", "Invalid phone number format.");

    public static readonly Error<object> IsAlreadyDeactivate = new("Update.Error", "Account already deactivate.");

    public static readonly Error<object> IsNotDeactivate = new("Update.Error", "Account is not deactivate.");

    public static readonly Error<object> ConfirmationFailed = new("Update.Error", "Account confirmation failed.");
}