﻿using DiwaTouch.Auth.Models;
using DiwaTouch.Shared.OperationResult;
using MediatR;

namespace DiwaTouch.Auth.Queries;

public record AuthenticateUserQuery(string Email, string Password) : IRequest<Result<(UserModel, string)>>;