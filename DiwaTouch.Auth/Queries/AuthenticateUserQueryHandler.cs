﻿using AutoMapper;
using DiwaTouch.Auth.Errors;
using DiwaTouch.Auth.Models;
using DiwaTouch.Auth.Persistence.Port.Contracts;
using DiwaTouch.Shared.Exceptions;
using DiwaTouch.Shared.OperationResult;
using MediatR;

namespace DiwaTouch.Auth.Queries;

internal sealed class AuthenticateUserQueryHandler(
    IAuthService authService,
    ITokenGenerator tokenGenerator,
    IMapper mapper
) : IRequestHandler<AuthenticateUserQuery, Result<(UserModel, string)>>
{
    public async Task<Result<(UserModel, string)>> Handle(AuthenticateUserQuery request,
        CancellationToken cancellationToken)
    {
        try
        {
            var user = await authService.AuthenticateAsync(request.Email, request.Password);
            var roles = await authService.GetRoleAsync(user);
            var token = tokenGenerator.GenerateJwToken(user, roles);
            
            //test a del (ça fonctionne)
            var testToken = await tokenGenerator.GenerateReactivateAccountTokenAsync(user, "hellotoken");
            Console.WriteLine(testToken);
            var isTrue = await tokenGenerator.VerifyReactivateAccountTokenAsync(user, "hellotoken", testToken);
            Console.WriteLine(isTrue);
            var isFalse = await tokenGenerator.VerifyReactivateAccountTokenAsync(user, "hellotoken", testToken+"aze");
            Console.WriteLine(isFalse);
            //fin du test

            return Result<(UserModel, string)>.Success(new((mapper.Map<UserModel>(user), token)));
        }
        catch (UnAuthorizedException)
        {
            return AuthError.InvalidCredentials;
        }
        catch (AccessRestrictionException)
        {
            return AccountRestrictionError.NotConfirmed;
        }
        catch (Exception e)
        {
            return Result<(UserModel, string)>.Failure(new(e.Message));
        }
    }
}