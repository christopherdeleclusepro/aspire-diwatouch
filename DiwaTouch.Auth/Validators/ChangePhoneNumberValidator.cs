﻿using DiwaTouch.Auth.Commands;
using FluentValidation;

namespace DiwaTouch.Auth.Validators;

public class ChangePhoneNumberValidator : AbstractValidator<ChangePhoneNumberCommand>
{
    public ChangePhoneNumberValidator()
    {
        RuleFor(u => u.NewPhoneNumber)
            .Matches(@"^(?:(?:\+(?:32|33)|00(?:32|33))[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)?[1-9]\d{8}$")
            .WithMessage("Invalid phone number format. \n");
    }
}