﻿using DiwaTouch.Auth.Commands;
using FluentValidation;

namespace DiwaTouch.Auth.Validators;

public class CreateUserValidator : AbstractValidator<CreateUserCommand>
{
    public CreateUserValidator()
    {
        RuleFor(u => u.Email)
            .NotNull()
            .WithMessage("Email cannot be null. \n")
            .NotEmpty()
            .WithMessage("Email cannot be empty. \n")
            .EmailAddress()
            .WithMessage("Invalid email format. \n");

        RuleFor(u => u.Password)
            .Matches(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?#&])[A-Za-z\d@$!%*?#&]{6,}$")
            .WithMessage(
                "Password must contain at least one lowercase letter, one uppercase letter, one digit, one special character (@$!%*?#&), and have a minimum length of 6 characters. \n"
            );

        RuleFor(u => u.FirstName)
            .NotNull()
            .WithMessage("First name cannot be null. \n")
            .MinimumLength(2)
            .WithMessage("First name must be at least 2 characters long. \n");

        RuleFor(u => u.LastName)
            .NotNull()
            .WithMessage("Last name cannot be null. \n")
            .MinimumLength(2)
            .WithMessage("Last name must be at least 2 characters long. \n");

        RuleFor(u => u.BirthDate)
            .NotNull()
            .WithMessage("Birth date cannot be null. \n")
            .NotEmpty()
            .WithMessage("Birth date cannot be empty. \n");

        RuleFor(u => u.Gender)
            .NotNull()
            .WithMessage("Gender cannot be null. \n")
            .MinimumLength(2)
            .WithMessage("Gender must be at least 2 characters long. \n");

        RuleFor(u => u.PhoneNumber)
            .Matches(@"^(?:(?:\+(?:32|33)|00(?:32|33))[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)?[1-9]\d{8}$")
            .WithMessage("Invalid phone number format. \n");
    }
}