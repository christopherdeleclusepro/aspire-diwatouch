﻿using DiwaTouch.Shared.OperationResult;
using MediatR;

namespace DiwaTouch.Auth.Commands;

public record ConfirmAccountCommand(string UserId, string ConfirmationToken) : IRequest<Result<object>>;