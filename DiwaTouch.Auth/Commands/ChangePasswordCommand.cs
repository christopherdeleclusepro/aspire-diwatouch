﻿using DiwaTouch.Shared.OperationResult;
using MediatR;

namespace DiwaTouch.Auth.Commands;

public record ChangePasswordCommand(string CurrentPassword) : IRequest<Result<object>>;