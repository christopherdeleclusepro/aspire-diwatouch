﻿using DiwaTouch.Shared.OperationResult;
using MediatR;

namespace DiwaTouch.Auth.Commands;

public record ChangePhoneNumberCommand(string NewPhoneNumber, string Email) : IRequest<Result<object>>;