﻿using DiwaTouch.Auth.Errors;
using DiwaTouch.Messaging.Port;
using DiwaTouch.Messaging.Port.Contracts;
using DiwaTouch.Auth.Persistence.Port.Contracts;
using DiwaTouch.Messaging.Port.Events.UserAuth;
using DiwaTouch.Messaging.Port.Events.UserAuth.Mailing;
using DiwaTouch.Shared.Exceptions;
using DiwaTouch.Shared.OperationResult;
using MediatR;

namespace DiwaTouch.Auth.Commands;

public class ReactivateAccountCommandHandler
    (IUserService userService, IEventPublisher publisher, IPublisher sender) 
    : IRequestHandler<ReactivateAccountCommand, Result<object>>
{
    public async Task<Result<object>> Handle(ReactivateAccountCommand request, CancellationToken cancellationToken)
    {
        try
        {
            if (await userService.IsAlreadyActive(request.Email))
                return UserError.IsNotDeactivate;

            await userService.UpdateIsDeletedAsync(request.Email, true);
            
            publisher.PublishEvent(QueueNames.EmailChangeAccountState,
                new SendEmailReactivateAccountEvent(request.Email));
            
            publisher.PublishEvent(QueueNames.UpdateIdentityUser,
                new ChangeIdentityUserStateEvent(request.Email, true));

            return Result<object>.Success(SuccessData<object>.None);
        }
        catch (InsertionException e)
        {
            return Result<object>.Failure(new(e.Message));
        }
        catch (Exception e)
        {
            return Result<object>.Failure(new(e.Message));
        }
    }
}