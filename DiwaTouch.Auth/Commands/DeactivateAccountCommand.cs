﻿using DiwaTouch.Shared.OperationResult;
using MediatR;

namespace DiwaTouch.Auth.Commands;

public record DeactivateAccountCommand(string Email) : IRequest<Result<object>>;