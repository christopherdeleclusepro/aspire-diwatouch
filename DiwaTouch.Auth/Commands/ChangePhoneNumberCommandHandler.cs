﻿using DiwaTouch.Auth.Persistence.Port.Contracts;
using DiwaTouch.Shared.Exceptions;
using DiwaTouch.Shared.OperationResult;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace DiwaTouch.Auth.Commands;

internal sealed class ChangePhoneNumberCommandHandler(
        IUserService userService,
        IValidator<ChangePhoneNumberCommand> validator,
        IHttpContextAccessor context
    )
    : IRequestHandler<ChangePhoneNumberCommand, Result<object>>
{
    public async Task<Result<object>> Handle(ChangePhoneNumberCommand request, CancellationToken cancellationToken)
    {
        try
        {
            await validator.ValidateAndThrowAsync(request, cancellationToken);
            await userService.UpdatePhoneNumberAsync(request.Email, request.NewPhoneNumber);

            return Result<object>.Success(SuccessData<object>.None);
        }
        catch (InsertionException e)
        {
            return Result<object>.Failure(new(e.Message));
        }
        catch (Exception e)
        {
            return Result<object>.Failure(new(e.Message));
        }
    }
}