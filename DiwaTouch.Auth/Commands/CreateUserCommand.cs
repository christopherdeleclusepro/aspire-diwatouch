﻿using DiwaTouch.Shared.OperationResult;
using MediatR;

namespace DiwaTouch.Auth.Commands;

public record CreateUserCommand(
    string Email,
    string Password,
    string FirstName,
    string LastName,
    string Gender,
    DateTime BirthDate,
    string PhoneNumber
) : IRequest<Result<object>>;