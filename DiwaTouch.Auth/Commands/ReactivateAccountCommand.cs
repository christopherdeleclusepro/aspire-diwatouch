﻿using DiwaTouch.Shared.OperationResult;
using MediatR;

namespace DiwaTouch.Auth.Commands;

public record ReactivateAccountCommand(string Email) : IRequest<Result<object>>;