﻿using DiwaTouch.Auth.Errors;
using DiwaTouch.Messaging.Port;
using DiwaTouch.Messaging.Port.Contracts;
using DiwaTouch.Auth.Persistence.Port.Contracts;
using DiwaTouch.Messaging.Port.Events.UserAuth;
using DiwaTouch.Shared.Exceptions;
using DiwaTouch.Shared.OperationResult;
using MediatR;

namespace DiwaTouch.Auth.Commands;

internal sealed class ConfirmAccountCommandHandler
    (IAuthService authService, IUserService userService, IEventPublisher publisher) : IRequestHandler<ConfirmAccountCommand, Result<object>>
{
    public async Task<Result<object>> Handle(ConfirmAccountCommand request, CancellationToken cancellationToken)
    {
        try
        {
            var user = await userService.GetUserByIdAsync(request.UserId);

            await authService.ConfirmEmailTokenAsync(user, Uri.UnescapeDataString(request.ConfirmationToken));
            publisher.PublishEvent(QueueNames.UpdateIdentityUser, new ChangeIdentityUserStateEvent(user.Email!, true));

            return Result<object>.Success(SuccessData<object>.None);
        }
        catch (SelectionException)
        {
            return UserError.IsNotFound;
        }
        catch (InsertionException)
        {
            return UserError.ConfirmationFailed;
        }
        catch (Exception e)
        {
            return Result<object>.Failure(new(e.Message));
        }
    }
}

