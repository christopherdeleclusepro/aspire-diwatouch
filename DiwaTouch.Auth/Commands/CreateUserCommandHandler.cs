﻿using DiwaTouch.Auth.Errors;
using DiwaTouch.Messaging.Port;
using DiwaTouch.Messaging.Port.Contracts;
using DiwaTouch.Auth.Persistence.Port.Abstractions;
using DiwaTouch.Auth.Persistence.Port.Contracts;
using DiwaTouch.Messaging.Port.Events.UserAuth;
using DiwaTouch.Messaging.Port.Events.UserAuth.Mailing;
using DiwaTouch.Shared.Exceptions;
using DiwaTouch.Shared.OperationResult;
using FluentValidation;
using MediatR;

namespace DiwaTouch.Auth.Commands;

internal sealed class CreateUserCommandHandler(
    IAuthService authService,
    IValidator<CreateUserCommand> validator,
    IEventPublisher publisher)
    : IRequestHandler<CreateUserCommand, Result<object>>
{
    public async Task<Result<object>> Handle(CreateUserCommand request, CancellationToken cancellationToken)
    {
        try
        {
            await validator.ValidateAndThrowAsync(request, cancellationToken);

            var user = await authService.CreateUserAsync(request.Email, request.Password, request.PhoneNumber);
            await authService.AssignRoleAsync(user?.Email!, Roles.Member);

            publisher.PublishEvent(QueueNames.EmailConfirmedAccount,
                new SendEmailConfirmationEvent(user.Id,
                    user.Email,
                    request.FirstName,
                    request.LastName,
                    await authService.GenerateEmailConfirmationTokenAsync(user)
                )
            );

            publisher.PublishEvent(QueueNames.CreateIdentityUser,
                new CreateIdentityUserEvent(
                    user.Email,
                    request.FirstName,
                    request.LastName,
                    request.Gender,
                    request.BirthDate
                )
            );

            return Result<object>.Success(SuccessData<object>.None);
        }
        catch (InsertionException)
        {
            return UserError.IsAlreadyExist;
        }
        catch (ValidationException e)
        {
            return Result<object>.Failure(new(e.Message));
        }
        catch (Exception e)
        {
            return Result<object>.Failure(new(e.Message));
        }
    }
}