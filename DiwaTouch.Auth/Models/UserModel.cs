﻿namespace DiwaTouch.Auth.Models;

public record UserModel(string Id, string UserName, string Email, string PhoneNumber);