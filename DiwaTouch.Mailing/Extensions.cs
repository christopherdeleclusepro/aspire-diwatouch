﻿using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace DiwaTouch.Mailing;

public static class Extensions
{
    public static void AddMailingDomain(this IServiceCollection service)
    {
        service.AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(Assembly.GetExecutingAssembly()));
    }
}