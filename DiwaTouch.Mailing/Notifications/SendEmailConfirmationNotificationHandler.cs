﻿using DiwaTouch.Mailing.Port.Abstractions;
using DiwaTouch.Mailing.Port.Contracts;
using DiwaTouch.Messaging.Port.Events.UserAuth.Mailing;
using DiwaTouch.Shared.Mailing.Abstractions;
using MediatR;

namespace DiwaTouch.Mailing.Notifications;

internal sealed class SendEmailConfirmationNotificationHandler
    (IAppEmailSender emailSender, IHtmlToStringParser htmlParser)
    : INotificationHandler<SendEmailConfirmationEvent>
{
    public async Task Handle(SendEmailConfirmationEvent notification, CancellationToken cancellationToken)
    {
        var subject = "DiwaTouch account registration confirmation.";

        var htmlContent = await htmlParser.ParseTemplateHtmlAsync(
            TemplateNames.EmailConfirmation,
            notification.UserId,
            notification.FirstName,
            notification.LastName,
            notification.ConfirmationToken
        );

        await emailSender.SendEmailAsync(
            $"{notification.FirstName} {notification.LastName}",
            notification.Email,
            subject,
            htmlContent
        );
    }
}