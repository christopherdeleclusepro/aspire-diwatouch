﻿using DiwaTouch.Mailing.Port.Contracts;
using DiwaTouch.Messaging.Port.Events.UserAuth.Mailing;
using DiwaTouch.Shared.Mailing.Abstractions;
using MediatR;

namespace DiwaTouch.Mailing.Notifications;

public class SendEmailReactivateAccountNotificationHandler
    (IAppEmailSender emailSender, IHtmlToStringParser htmlToStringParser)
    : INotificationHandler<SendEmailReactivateAccountEvent>
{
    public async Task Handle(SendEmailReactivateAccountEvent notification, CancellationToken cancellationToken)
    {
        Console.WriteLine(notification.Email);
    }
}