﻿using DiwaTouch.Mailing.Port.Contracts;
using DiwaTouch.Messaging.Port.Events.UserAuth.Mailing;
using DiwaTouch.Shared.Mailing.Abstractions;
using MediatR;

namespace DiwaTouch.Mailing.Notifications;

public class SendEmailDeactivateAccountNotificationHandler
    (IAppEmailSender emailSender, IHtmlToStringParser htmlToStringParser)
    : INotificationHandler<SendEmailDeactivateAccountEvent>
{
    public async Task Handle(SendEmailDeactivateAccountEvent notification, CancellationToken cancellationToken)
    {
        Console.WriteLine(notification.Email);
    }
}