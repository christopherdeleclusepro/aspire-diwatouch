﻿using DiwaTouch.Messaging.Port.Contracts;
using DiwaTouch.Messaging.Port.Services;
using DiwaTouch.Middleware.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DiwaTouch.Auth.Infrastructure;

public static class Extensions
{
    public static void AddAuthInfra(this IServiceCollection service, IConfiguration configuration)
    {
        service.AddSingleton<IEventPublisher, RabbitmqEventPublisherService>();
        service.AddSingleton<IEventConsumer, RabbitmqEventConsumerService>();
        
        service.AddCustomAuthenticationAndAuthorization(configuration);
    }

    public static void UseAuthInfra(this WebApplication app)
    {
        app.UseCustomAuthenticationAndAuthorization();
    }
}