﻿using DiwaTouch.Shared.Mailing;
using DiwaTouch.Shared.Mailing.Abstractions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using sib_api_v3_sdk.Api;
using sib_api_v3_sdk.Model;
using Task = System.Threading.Tasks.Task;

namespace DiwaTouch.Mailing.Infrastructure.Services;

public sealed class AppEmailSender(IOptions<MailingConfig> options, ILogger<AppEmailSender> logger) : IAppEmailSender
{
    public async Task SendEmailAsync(string toName, string toEmail, string subject, string htmlContent)
    {
        ArgumentException.ThrowIfNullOrEmpty(toName);
        ArgumentException.ThrowIfNullOrEmpty(toEmail);
        ArgumentException.ThrowIfNullOrEmpty(subject);
        ArgumentException.ThrowIfNullOrEmpty(htmlContent);
        
        var smtpEmailFrom = new SendSmtpEmailSender(options.Value.SenderName, options.Value.SenderEmail);
        var smtpEmailToList = new List<SendSmtpEmailTo> { new(toEmail, toName) };

        try
        {
            var sendSmtpEmail = new SendSmtpEmail(smtpEmailFrom, smtpEmailToList, null, null, htmlContent, null, subject);
            var response = await new TransactionalEmailsApi().SendTransacEmailAsync(sendSmtpEmail);

            logger.LogInformation("Email send successfully: {result}", response.ToJson());
        }
        catch (ArgumentNullException e)
        {
            logger.LogError(e, e.Message);
        }
        catch (Exception e)
        {
            logger.LogError(e, "An error was occured when sending an email.");
        }
    }
}
