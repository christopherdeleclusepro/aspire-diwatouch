﻿using DiwaTouch.Mailing.Port.Abstractions;
using DiwaTouch.Mailing.Port.Contracts;

namespace DiwaTouch.Mailing.Infrastructure.Services;

public class HtmlToStringParser : IHtmlToStringParser
{
    public async Task<string> ParseTemplateHtmlAsync(
        TemplateNames templateNames,
        string userId,
        string firstName,
        string lastName,
        string token) => (await File.ReadAllTextAsync(GetTemplatePath($"{templateNames.ToString()}.html")))
        .Replace("{firstName}", firstName)
        .Replace("{lastName}", lastName)
        .Replace("{userId}", userId)
        .Replace("{token}", Uri.EscapeDataString(token));

    private string GetTemplatePath(string templateName) =>
        Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates"), templateName);
}