﻿using DiwaTouch.Messaging.Port;
using DiwaTouch.Messaging.Port.Abstractions;
using DiwaTouch.Messaging.Port.Contracts;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace DiwaTouch.Mailing.Infrastructure.Services.HostedServices;

public class EmailChangeAccountStateQueue(
    IServiceScopeFactory serviceScopeFactory,
    IEventConsumer eventConsumer,
    ILogger<EmailChangeAccountStateQueue> logger
) : GenericQueueHostedService<EmailChangeAccountStateQueue>(serviceScopeFactory, eventConsumer, QueueNames.EmailChangeAccountState, logger);