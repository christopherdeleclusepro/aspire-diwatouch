﻿using DiwaTouch.Messaging.Port;
using DiwaTouch.Messaging.Port.Abstractions;
using DiwaTouch.Messaging.Port.Contracts;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace DiwaTouch.Mailing.Infrastructure.Services.HostedServices;

public class EmailConfirmedAccountQueue(
    IServiceScopeFactory serviceScopeFactory,
    IEventConsumer eventConsumer,
    ILogger<EmailConfirmedAccountQueue> logger
) : GenericQueueHostedService<EmailConfirmedAccountQueue>(serviceScopeFactory, eventConsumer, QueueNames.EmailConfirmedAccount, logger);