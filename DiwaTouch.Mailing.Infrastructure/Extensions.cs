﻿using DiwaTouch.Mailing.Infrastructure.Services;
using DiwaTouch.Mailing.Infrastructure.Services.HostedServices;
using DiwaTouch.Mailing.Port.Contracts;
using DiwaTouch.Messaging.Port.Contracts;
using DiwaTouch.Messaging.Port.Services;
using DiwaTouch.Shared.Mailing;
using DiwaTouch.Shared.Mailing.Abstractions;
using Microsoft.Extensions.DependencyInjection;
using sib_api_v3_sdk.Client;

namespace DiwaTouch.Mailing.Infrastructure;

public static class Extensions
{
    public static void AddMailingInfra(this IServiceCollection service, MailingConfig mailingOptions)
    {
        Configuration.Default.ApiKey.Add("api-key", mailingOptions.ApiKey);

        service.Configure<MailingConfig>(opt =>
        {
            opt.SenderName = mailingOptions.SenderName;
            opt.SenderEmail = mailingOptions.SenderEmail;
        });
        
        service.AddHostedService<EmailConfirmedAccountQueue>();

        service.AddScoped<IAppEmailSender, AppEmailSender>();
        service.AddTransient<IHtmlToStringParser, HtmlToStringParser>();
        
        service.AddSingleton<IEventPublisher, RabbitmqEventPublisherService>();
        service.AddSingleton<IEventConsumer, RabbitmqEventConsumerService>();
    }
}