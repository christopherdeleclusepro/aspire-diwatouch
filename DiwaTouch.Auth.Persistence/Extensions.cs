﻿using DiwaTouch.Auth.Persistence.Entities;
using DiwaTouch.Auth.Persistence.Port.Contracts;
using DiwaTouch.Auth.Persistence.Providers;
using DiwaTouch.Auth.Persistence.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace DiwaTouch.Auth.Persistence;

public static class Extensions
{
    public static void AddAuthPersistence(
        this WebApplicationBuilder builder,
        IConfiguration configuration
    )
    {
        builder.Services.Configure<JwtOptions>(configuration.GetSection("JwtOptions"));

        builder.AddSqlServerDbContext<AppDbContext>("DiwaTouchIdentity");
        builder.Services.AddIdentityCore<AppUser>(opt =>
                {
                    opt.User.RequireUniqueEmail = true;
                    opt.SignIn.RequireConfirmedEmail = true;

                    opt.Password.RequiredLength = 6;
                    opt.Password.RequiredUniqueChars = 1;
                    opt.Password.RequireDigit = true;
                    opt.Password.RequireLowercase = true;
                    opt.Password.RequireUppercase = true;
                }
            )
            .AddRoles<IdentityRole>()
            .AddDefaultTokenProviders()
            .AddTokenProvider<AccountTokenProvider<AppUser>>(AppTokenProviderName.AccountToken)
            .AddEntityFrameworkStores<AppDbContext>();
        
        builder.Services.AddScoped<IAuthService, AuthService>();
        builder.Services.AddScoped<ITokenGenerator, TokenGenerator>();
        builder.Services.AddScoped<IUserService, UserService>();
    }

    public static void UseAuthPersistence(this WebApplication app)
    {
        if (app.Environment.IsDevelopment()) 
            app.ApplyMigrations();
    }

    private static void ApplyMigrations(this WebApplication app)
    {
        using var scope = app.Services.CreateScope();
        var db = scope.ServiceProvider.GetRequiredService<AppDbContext>();

        if (db.Database.GetPendingMigrations()
            .Any()) db.Database.Migrate();
    }
}