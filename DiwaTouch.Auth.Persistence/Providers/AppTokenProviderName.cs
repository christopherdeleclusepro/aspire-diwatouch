﻿namespace DiwaTouch.Auth.Persistence.Providers;

public abstract class AppTokenProviderName
{
    public const string AccountToken = "accountToken";
}

public abstract class AppTokenProviderPurpose
{
    public const string ReactivateAccount = "reactivateAccount";
}