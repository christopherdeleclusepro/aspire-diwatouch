﻿using DiwaTouch.Auth.Persistence.Entities;
using DiwaTouch.Auth.Persistence.Port.Contracts;
using DiwaTouch.Shared.Exceptions;
using Microsoft.AspNetCore.Identity;

namespace DiwaTouch.Auth.Persistence.Services;

public class UserService(UserManager<AppUser> userManager) : IUserService
{
    public async Task UpdatePhoneNumberAsync(string email, string phoneNumber)
    {
        var user = await GetUserByEmailAsync(email);

        if (user.PhoneNumber == phoneNumber)
            throw InsertionException.ForSameValue(nameof(phoneNumber));

        var result = await userManager.SetPhoneNumberAsync((AppUser)user, phoneNumber);

        if (!result.Succeeded && result.Errors.Any())
            throw InsertionException.ForFailed(string.Join("\n",
                    result.Errors.Select(err => $"{err.Code}: {err.Description}")
                )
            );
    }

    public async Task<bool> IsAlreadyActive(string email) => (await GetUserByEmailAsync(email)).IsActive;

    public async Task UpdateIsDeletedAsync(string email, bool isActive)
    {
        var user = await GetUserByEmailAsync(email);

        user.IsActive = isActive;

        var result = await userManager.UpdateAsync((AppUser)user);

        if (!result.Succeeded && result.Errors.Any())
            throw InsertionException.ForFailed(string.Join("\n",
                    result.Errors.Select(err => $"${err.Code}: {err.Description}")
                )
            );
    }

    public async Task UpdatePasswordAsync(string email, string currentPassword, string newPassword)
    {
        var user = await GetUserByEmailAsync(email);
        var result = await userManager.ChangePasswordAsync((AppUser)user, currentPassword, newPassword);

        if (!result.Succeeded && result.Errors.Any())
            throw InsertionException.ForFailed(string.Join("\n",
                    result.Errors.Select(err => $"{err.Code}: {err.Description}")
                )
            );
    }

    public async Task<IAppUser> GetUserByEmailAsync(string email) =>
        await userManager.FindByEmailAsync(email) ?? throw SelectionException.ForNotFound();

    public async Task<IAppUser> GetUserByIdAsync(string id) =>
        await userManager.FindByIdAsync(id) ?? throw SelectionException.ForNotFound();
}