﻿using DiwaTouch.Auth.Persistence.Entities;
using DiwaTouch.Auth.Persistence.Port.Abstractions;
using DiwaTouch.Auth.Persistence.Port.Contracts;
using DiwaTouch.Shared.Exceptions;
using Microsoft.AspNetCore.Identity;

namespace DiwaTouch.Auth.Persistence.Services;

internal sealed class AuthService(
    UserManager<AppUser> userManager,
    RoleManager<IdentityRole> roleManager
) : IAuthService
{
    public async Task<IAppUser?> CreateUserAsync(string email, string password, string phoneNumber)
    {
        var user = new AppUser { UserName = email, Email = email, PhoneNumber = phoneNumber, IsActive = false };

        var result = await userManager.CreateAsync(user, password);

        if (result.Errors.Any())
            throw InsertionException.ForFailed(string.Join("\n",
                    result.Errors.Select(err => $"{err.Code}: {err.Description}")
                )
            );

        return await userManager.FindByEmailAsync(email);
    }

    public async Task<IAppUser> AuthenticateAsync(string email, string password)
    {
        var user = await userManager.FindByEmailAsync(email);

        if (user is null || !await userManager.CheckPasswordAsync(user, password))
            throw UnAuthorizedException.ForInvalidCredentials();
        
        if (!await userManager.IsEmailConfirmedAsync(user))
            throw AccessRestrictionException.ForNotVerifiedAccount();

        return user;
    }

    public async Task<bool> AssignRoleAsync(string email, Roles role)
    {
        var user = await userManager.FindByEmailAsync(email);

        if (user is null) return false;

        if (!await roleManager.RoleExistsAsync(role.ToString())) await roleManager.CreateAsync(new(role.ToString()));

        await userManager.AddToRoleAsync(user, role.ToString());

        return true;
    }

    public async Task<IList<string>> GetRoleAsync(IAppUser user) => await userManager.GetRolesAsync((AppUser)user);

    public async Task<string> GenerateEmailConfirmationTokenAsync(IAppUser user) =>
       await userManager.GenerateEmailConfirmationTokenAsync((AppUser)user);

    public async Task<bool> IsEmailConfirmedAsync(IAppUser user) => await userManager.IsEmailConfirmedAsync((AppUser)user);

    public async Task ConfirmEmailTokenAsync(IAppUser user, string token)
    {
        var result = await userManager.ConfirmEmailAsync((AppUser)user, token);
        
        if (result.Errors.Any())
            throw InsertionException.ForFailed(string.Join("\n",
                    result.Errors.Select(err => $"{err.Code}: {err.Description}")
                )
            );
    }
}
