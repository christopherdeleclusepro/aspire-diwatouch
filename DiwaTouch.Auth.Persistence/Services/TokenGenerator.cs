﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using DiwaTouch.Auth.Persistence.Entities;
using DiwaTouch.Auth.Persistence.Port.Contracts;
using DiwaTouch.Auth.Persistence.Providers;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace DiwaTouch.Auth.Persistence.Services;

public sealed class TokenGenerator
    (IOptions<JwtOptions> jwtOptions, UserManager<AppUser> userManager) : ITokenGenerator
{
    public string GenerateJwToken(IAppUser user, IEnumerable<string> roles)
    {
        JwtSecurityTokenHandler tokenHandler = new();

        var key = Encoding.UTF8.GetBytes(jwtOptions.Value.Secret);

        var claims = new List<Claim>
        {
            new(JwtRegisteredClaimNames.Jti, user.Id!),
            new(JwtRegisteredClaimNames.Email, user.Email!),
            new(JwtRegisteredClaimNames.Name, user.UserName!)
        };

        claims.AddRange(roles.Select(r => new Claim(ClaimTypes.Role, r)));

        SecurityTokenDescriptor tokenDescriptor = new()
        {
            Issuer = jwtOptions.Value.Issuer,
            Audience = jwtOptions.Value.Audience,
            Subject = new ClaimsIdentity(claims),
            Expires = DateTime.UtcNow.AddDays(1),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                SecurityAlgorithms.HmacSha256Signature
            )
        };

        var token = tokenHandler.CreateToken(tokenDescriptor);

        return tokenHandler.WriteToken(token);
    }

    public async Task<string> GenerateReactivateAccountTokenAsync(IAppUser user, string provider) =>
        await userManager.GenerateUserTokenAsync((AppUser)user, AppTokenProviderName.AccountToken, AppTokenProviderPurpose.ReactivateAccount);

    public async Task<bool> VerifyReactivateAccountTokenAsync(IAppUser user, string provider, string token) =>
        await userManager.VerifyUserTokenAsync((AppUser)user, AppTokenProviderName.AccountToken, AppTokenProviderPurpose.ReactivateAccount, token);
}