﻿using DiwaTouch.Auth.Persistence.Port.Contracts;
using Microsoft.AspNetCore.Identity;

namespace DiwaTouch.Auth.Persistence.Entities;

public class AppUser : IdentityUser, IAppUser
{
    public bool IsActive { get; set; }
}
