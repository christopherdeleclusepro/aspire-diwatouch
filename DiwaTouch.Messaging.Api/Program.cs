using DiwaTouch.Messaging.DependencyInjections;

var builder = WebApplication.CreateBuilder(args);

builder.AddServiceDefaults();
builder.AddRabbitMQ("messaging");

// Add services to the container.
builder.Services.AddMessagingServices();

var app = builder.Build();

app.MapDefaultEndpoints();

// Configure the HTTP request pipeline.
app.UseHttpsRedirection();

app.Run();
