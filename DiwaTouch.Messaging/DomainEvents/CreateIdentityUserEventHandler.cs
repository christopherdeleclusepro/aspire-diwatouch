﻿using AutoMapper;
using DiwaTouch.Messaging.Port;
using DiwaTouch.Messaging.Port.Contracts;
using DiwaTouch.Messaging.Port.Events.UserAuth;
using DiwaTouch.Messaging.Port.Events.UserProfile;
using MediatR;

namespace DiwaTouch.Messaging.DomainEvents;

internal sealed class CreateIdentityUserEventHandler(IEventPublisher eventPublisher)
    : INotificationHandler<CreateIdentityUserEvent>
{
    public async Task Handle(CreateIdentityUserEvent notification, CancellationToken cancellationToken)
    {
        eventPublisher.PublishEvent(QueueNames.CreateUserProfile,
            new CreateUserProfileEvent(
                notification.Email,
                notification.FirstName,
                notification.LastName,
                notification.Gender,
                notification.BirthDate)
        );
    }
}