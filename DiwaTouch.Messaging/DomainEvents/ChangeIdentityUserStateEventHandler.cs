﻿using DiwaTouch.Messaging.Port;
using DiwaTouch.Messaging.Port.Contracts;
using DiwaTouch.Messaging.Port.Events.UserAuth;
using DiwaTouch.Messaging.Port.Events.UserProfile;
using MediatR;

namespace DiwaTouch.Messaging.DomainEvents;

internal sealed class ChangeIdentityUserStateEventHandler(IEventPublisher eventPublisher)
    : INotificationHandler<ChangeIdentityUserStateEvent>
{
    public async Task Handle(ChangeIdentityUserStateEvent notification, CancellationToken cancellationToken)
    {
        eventPublisher.PublishEvent(QueueNames.UpdateUserProfile,
            new ChangeUserProfileStateEvent(notification.Email, notification.IsActive));
    }
}