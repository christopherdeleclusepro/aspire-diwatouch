﻿using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace DiwaTouch.Messaging;

public static class Extensions
{
    public static void AddMessagingDomain(this IServiceCollection service)
    {
        service.AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(Assembly.GetExecutingAssembly()));
        //service.AddAutoMapper(cfg => cfg.AddMaps(Assembly.GetExecutingAssembly()));
    }
}