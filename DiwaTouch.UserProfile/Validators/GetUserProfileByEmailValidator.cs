﻿using DiwaTouch.UserProfile.Queries;
using FluentValidation;

namespace DiwaTouch.UserProfile.Validators;

public class GetUserProfileByEmailValidator : AbstractValidator<GetUserProfileByEmailQuery>
{
    public GetUserProfileByEmailValidator() =>
        RuleFor(up => up.Email)
            .NotNull()
            .NotEmpty()
            .EmailAddress();
}
