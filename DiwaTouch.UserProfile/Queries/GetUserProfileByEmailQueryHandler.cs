﻿using AutoMapper;
using DiwaTouch.Shared.OperationResult;
using DiwaTouch.UserProfile.Errors;
using DiwaTouch.UserProfile.Models;
using DiwaTouch.UserProfile.Persistence.Port.Contracts;
using FluentValidation;
using MediatR;

namespace DiwaTouch.UserProfile.Queries;

internal sealed class GetUserProfileByEmailQueryHandler(
        IUserProfileRepository userProfileRepository,
        IMapper mapper,
        IValidator<GetUserProfileByEmailQuery> validator
    )
    : IRequestHandler<GetUserProfileByEmailQuery, Result<UserProfileModel>>
{
    public async Task<Result<UserProfileModel>> Handle(GetUserProfileByEmailQuery request, CancellationToken cancellationToken)
    {
        try
        {
            await validator.ValidateAndThrowAsync(request, cancellationToken);

            var userProfile = mapper.Map<UserProfileModel>(await userProfileRepository.GetProfileByEmailAsync(request.Email));

            return Result<UserProfileModel>.Success(new SuccessData<UserProfileModel>(userProfile));
        }
        catch (InvalidOperationException)
        {
            return UserProfileError.UserNotFound;
        }
        catch (ValidationException e)
        {
            return Result<UserProfileModel>.Failure(new(e.Message));
        }
        catch (Exception e)
        {
            return Result<UserProfileModel>.Failure(new(e.Message));
        }
    }
}