﻿using DiwaTouch.Shared.OperationResult;
using DiwaTouch.UserProfile.Models;
using MediatR;

namespace DiwaTouch.UserProfile.Queries;

public record GetUserProfileByEmailQuery(string Email) : IRequest<Result<UserProfileModel>>;