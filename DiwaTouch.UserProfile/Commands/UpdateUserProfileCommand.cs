﻿using DiwaTouch.Shared.OperationResult;
using DiwaTouch.UserProfile.Models;
using MediatR;

namespace DiwaTouch.UserProfile.Commands;

public class UpdateUserProfileCommand : IRequest<Result<UserProfileModel>>
{
    public string? FirstName { get; set; }
    
    public string? LastName { get; set; }
    
    public string? Country { get; set; }
    
    public bool? HasPartner { get; set; }
    
    public string? Gender { get; set; }
    
    public string? ProfilePicture { get; set; }
    
    public string? BackdropImage { get; set; }
}