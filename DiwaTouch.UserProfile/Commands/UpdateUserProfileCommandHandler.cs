﻿using DiwaTouch.Shared.OperationResult;
using DiwaTouch.UserProfile.Models;
using MediatR;

namespace DiwaTouch.UserProfile.Commands;

internal sealed class UpdateUserProfileCommandHandler : IRequestHandler<UpdateUserProfileCommand, Result<UserProfileModel>>
{
    public Task<Result<UserProfileModel>> Handle(UpdateUserProfileCommand request, CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }
}