﻿namespace DiwaTouch.UserProfile.Models;

public class UserProfileModel
{
    public string? Id { get; set; }
    
    public string? Email { get; set; }
    
    public string? FirstName { get; set; }
    
    public string? LastName { get; set; }
    
    public string? Gender { get; set; }
    
    public DateTime? BirthDate { get; set; }
    
    public string? Country { get; set; }
    
    public bool? HasPartner { get; set; }
    
    public string? ProfilePicture { get; set; }
    
    public string? BackdropImage { get; set; }
}
