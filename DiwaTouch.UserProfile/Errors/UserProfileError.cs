﻿using DiwaTouch.Shared.OperationResult;
using DiwaTouch.UserProfile.Models;

namespace DiwaTouch.UserProfile.Errors;

public static class UserProfileError
{
    public static readonly Error<UserProfileModel> UserNotFound = new("Query.Error", "User was not found.");
}
