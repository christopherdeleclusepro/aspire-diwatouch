﻿using DiwaTouch.Messaging.Port.Events.UserProfile;
using DiwaTouch.UserProfile.Persistence.Port.Contracts;
using DiwaTouch.UserProfile.Persistence.Port.Entities;
using MediatR;

namespace DiwaTouch.UserProfile.DomainEvents;

internal sealed class CreateUserProfileEventHandler(IUserProfileRepository userProfileRepository)
    : INotificationHandler<CreateUserProfileEvent>
{
    public async Task Handle(CreateUserProfileEvent notification, CancellationToken cancellationToken)
    {
        var userProfileEntity = new UserProfileEntity()
        {
            Email = notification.Email,
            FirstName = notification.FirstName,
            LastName = notification.LastName,
            Gender = notification.Gender,
            BirthDate = notification.BirthDate,
            IsDeleted = true
        };
        
        await userProfileRepository.CreateProfileAsync(userProfileEntity);
    }
}
