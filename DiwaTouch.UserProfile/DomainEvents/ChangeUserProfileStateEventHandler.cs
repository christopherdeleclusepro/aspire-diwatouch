﻿using DiwaTouch.Messaging.Port.Events.UserProfile;
using DiwaTouch.UserProfile.Persistence.Port.Contracts;
using MediatR;

namespace DiwaTouch.UserProfile.DomainEvents;

public class ChangeUserProfileStateEventHandler(IUserProfileRepository userProfileRepository)
    : INotificationHandler<ChangeUserProfileStateEvent>
{
    public async Task Handle(ChangeUserProfileStateEvent notification, CancellationToken cancellationToken)
    {
        await userProfileRepository.UpdateIsDeletedAsync(notification.Email, notification.IsActive);
    }
}