﻿using AutoMapper;
using DiwaTouch.UserProfile.Models;
using DiwaTouch.UserProfile.Persistence.Port.Entities;

namespace DiwaTouch.UserProfile.Mappers;

public class InfrastructureToApplicationMapping : Profile
{
    public InfrastructureToApplicationMapping()
    {
        CreateMap<UserProfileEntity, UserProfileModel>();
    }
}
