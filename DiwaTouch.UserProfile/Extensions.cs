﻿using System.Reflection;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;

namespace DiwaTouch.UserProfile;

public static class Extensions
{
    public static void AddUserProfileApplication(this IServiceCollection service)
    {
        service.AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(Assembly.GetExecutingAssembly()));
        service.AddAutoMapper(cfg => cfg.AddMaps(Assembly.GetExecutingAssembly()));
        service.AddValidatorsFromAssemblyContaining(typeof(Extensions));
    }
}
