﻿using DiwaTouch.Messaging.Port;
using DiwaTouch.Messaging.Port.Abstractions;
using DiwaTouch.Messaging.Port.Contracts;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace DiwaTouch.Messaging.Infrastructure.Services.HostedServices;

public class UserCreatedQueueService(
    IServiceScopeFactory serviceScopeFactory,
    IEventConsumer eventConsumer,
    ILogger<UserCreatedQueueService> logger
) : GenericQueueHostedService<UserCreatedQueueService>(serviceScopeFactory, eventConsumer, QueueNames.CreateIdentityUser, logger);
