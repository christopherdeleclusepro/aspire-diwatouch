﻿using DiwaTouch.Messaging.Port;
using DiwaTouch.Messaging.Port.Abstractions;
using DiwaTouch.Messaging.Port.Contracts;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace DiwaTouch.Messaging.Infrastructure.Services.HostedServices;

public class UserUpdatedQueueService(
    IServiceScopeFactory serviceScopeFactory,
    IEventConsumer eventConsumer,
    ILogger<UserUpdatedQueueService> logger
) : GenericQueueHostedService<UserUpdatedQueueService>(serviceScopeFactory, eventConsumer, QueueNames.UpdateIdentityUser, logger);
