﻿using DiwaTouch.Messaging.Infrastructure.Services.HostedServices;
using DiwaTouch.Messaging.Port.Contracts;
using DiwaTouch.Messaging.Port.Services;
using Microsoft.Extensions.DependencyInjection;

namespace DiwaTouch.Messaging.Infrastructure;

public static class Extensions
{
    public static void AddMessagingInfrastructure(this IServiceCollection service)
    {
        service.AddHostedService<UserCreatedQueueService>();
        service.AddHostedService<UserUpdatedQueueService>();
        
        service.AddSingleton<IEventPublisher, RabbitmqEventPublisherService>();
        service.AddSingleton<IEventConsumer, RabbitmqEventConsumerService>();
    }
}
