﻿using DiwaTouch.Auth.Persistence.Port.Abstractions;

namespace DiwaTouch.Auth.Persistence.Port.Contracts;

public interface IAuthService
{
    Task<IAppUser?> CreateUserAsync(string email, string password, string phoneNumber);
    
    Task<IAppUser> AuthenticateAsync(string email, string password);
    
    Task<bool> AssignRoleAsync(string email, Roles role);
    
    Task<IList<string>> GetRoleAsync(IAppUser user);
    
    Task<string> GenerateEmailConfirmationTokenAsync(IAppUser user);
    
    Task<bool> IsEmailConfirmedAsync(IAppUser user);
    
    Task ConfirmEmailTokenAsync(IAppUser user, string token);
}
