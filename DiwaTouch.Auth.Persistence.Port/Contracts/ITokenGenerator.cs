﻿namespace DiwaTouch.Auth.Persistence.Port.Contracts;

public interface ITokenGenerator
{
    string GenerateJwToken(IAppUser user, IEnumerable<string> roles);

    Task<string> GenerateReactivateAccountTokenAsync(IAppUser user, string provider);

    Task<bool> VerifyReactivateAccountTokenAsync(IAppUser user, string provider, string token);
}
