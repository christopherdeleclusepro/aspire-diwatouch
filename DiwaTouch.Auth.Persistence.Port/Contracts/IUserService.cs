﻿namespace DiwaTouch.Auth.Persistence.Port.Contracts;

public interface IUserService
{
    Task<IAppUser> GetUserByEmailAsync(string email);
    
    Task<IAppUser> GetUserByIdAsync(string id);

    Task UpdatePhoneNumberAsync(string email, string phoneNumber);
    
    Task<bool> IsAlreadyActive(string email);
    
    Task UpdateIsDeletedAsync(string email, bool isActive);
    
    Task UpdatePasswordAsync(string email, string currentPassword, string newPassword);
}
