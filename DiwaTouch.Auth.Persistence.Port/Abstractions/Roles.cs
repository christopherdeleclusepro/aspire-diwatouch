﻿namespace DiwaTouch.Auth.Persistence.Port.Abstractions;

public enum Roles
{
    Admin,
    Member
}
