﻿using DiwaTouch.UserProfile.Persistence;
using DiwaTouch.UserProfile.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DiwaTouch.UserProfile.DependencyInjections;

public static class Extensions
{
    public static void AddUserProfileDependencyInjections(this IServiceCollection service, IConfiguration configuration)
    {
        service.AddUserProfileInfrastructure(configuration);
        service.AddUserProfilePersistence();
        service.AddUserProfileApplication();
    }
}