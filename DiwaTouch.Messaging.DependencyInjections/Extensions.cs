﻿using DiwaTouch.Messaging.Infrastructure;
using Microsoft.Extensions.DependencyInjection;

namespace DiwaTouch.Messaging.DependencyInjections;

public static class Extensions
{
    public static void AddMessagingServices(this IServiceCollection service)
    {
        service.AddMessagingInfrastructure();
        service.AddMessagingDomain();
    }
}