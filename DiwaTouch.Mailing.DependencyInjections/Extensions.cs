﻿using DiwaTouch.Mailing.Infrastructure;
using DiwaTouch.Shared.Mailing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DiwaTouch.Mailing.DependencyInjections;

public static class Extensions
{
    public static void AddMailingService(this IServiceCollection service, IConfiguration configuration)
    {
        var mailingConfig = configuration.GetSection("mailing").Get<MailingConfig>();

        service.AddMailingDomain();
        service.AddMailingInfra(mailingConfig);
    }
}