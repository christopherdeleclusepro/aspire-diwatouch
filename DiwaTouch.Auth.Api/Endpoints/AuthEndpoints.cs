﻿using Carter;
using DiwaTouch.Middleware.Authorization;
using DiwaTouch.Auth.Commands;
using DiwaTouch.Auth.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DiwaTouch.Auth.Api.Endpoints;

public class AuthEndpoints : ICarterModule
{
    public void AddRoutes(IEndpointRouteBuilder app)
    {
        var group = app.MapGroup("auth");

        group.MapPost("local/register",
            async (CreateUserCommand command, ISender sender) =>
            {
                var result = await sender.Send(command);

                return result.IsSuccess
                    ? Results.NoContent()
                    : Results.BadRequest($"[{result.Error.Code}]: {result.Error.Description ?? string.Empty}");
            }
        );

        group.MapPost("local/login",
            async (AuthenticateUserQuery query, ISender sender) =>
            {
                var result = await sender.Send(query);

                if (!result.IsSuccess && result.Error.Code.Contains("Access.Denied"))
                    return Results.BadRequest($"[{result.Error.Code}]: {result.Error.Description ?? string.Empty}");

                return result.IsSuccess
                    ? Results.Ok(new { User = result.Data?.Items.Item1, Token = result.Data?.Items.Item2 })
                    : Results.Unauthorized();
            }
        );

        group.MapGet("local/confirm-account/userId={id}&token={confirmationToken}",
            async (string id, string confirmationToken, ISender sender) =>
            {
                var result = await sender.Send(new ConfirmAccountCommand(id, confirmationToken));

                return result.IsSuccess
                    ? Results.NoContent()
                    : Results.BadRequest($"[{result.Error.Code}]: {result.Error.Description ?? string.Empty}");
            }
        );

        group.MapPut("change-phone-number",
                async ([FromBody]string phoneNumber, ISender sender, HttpContext context) =>
                {
                    var user = context.GetUserClaims();
                    var result = await sender.Send(new ChangePhoneNumberCommand(phoneNumber, user.Email));

                    return result.IsSuccess
                        ? Results.NoContent()
                        : Results.BadRequest($"[{result.Error.Code}]: {result.Error.Description ?? string.Empty}");
                }
            )
            .RequireAuthorization();

        group.MapPut("deactivate-account",
                async (ISender sender, HttpContext context) =>
                {
                    var user = context.GetUserClaims();
                    var result = await sender.Send(new DeactivateAccountCommand(user.Email));

                    return result.IsSuccess
                        ? Results.NoContent()
                        : Results.BadRequest($"[{result.Error.Code}]: {result.Error.Description ?? string.Empty}");
                }
            )
            .RequireAuthorization();

        group.MapPut("reactivate-account",
                async (ISender sender, HttpContext context) =>
                {
                    var userClaims = context.GetUserClaims();
                    var result = await sender.Send(new ReactivateAccountCommand(userClaims.Email));

                    return result.IsSuccess
                        ? Results.NoContent()
                        : Results.BadRequest($"[{result.Error.Code}]: {result.Error.Description ?? string.Empty}");
                }
            )
            .RequireAuthorization();
    }
}